# 队列 Queue

#### 一、内存概念

先存进去的数据会先取出来，是 FIFO（First In First Out，**先进先出**）。

![image-20220924202757255](./assets/image-20220924202757255.png)

#### 二、程序操作

##### 1、创建队列。

```python
# Queue()创建一个空队列。它不需要参数，且会返回一个空队列。
# enqueue(item)在队列的尾部添加一个元素。它需要一个元素作为参数，不返回任何值。
# dequeue()从队列的头部移除一个元素。它不需要参数，且会返回一个元素，并修改队列的内容。
# isEmpty()检查队列是否为空。它不需要参数，且会返回一个布尔值。
# size()返回队列中元素的数目。它不需要参数，且会返回一个整数。

class Queue:
    '''队列类，用列表实现；list[0]为头部，list[-1]为尾部；添加在头部，移除在尾部'''
    def __init__(self):
        self.items = []
    def isEmpty(self):
        return self.items == []
    def enqueue(self, item):
        self.items.insert(0, item)
    def dequeue(self):
        return self.items.pop()
    def size(self):
        return len(self.items)
```

```python
class LinkedListQueue():
    '''
        链表实现队列
    '''
    class LNode: #链表节点
        def __init__(self, data=None, _next=None):
            self.data = data
            self.next = _next

    def __init__(self, lst=None):
        if isinstance(lst, list):  #列表转队列
            self.size = len(lst)
            self.tail = self.LNode(lst[-1])
            head=self.tail
            for x in range(self.size-1):
                N=self.LNode(lst[self.size-2-x],head)
                head=N
            self.head = head
        else:
            self.head = None #头
            self.tail = None #尾
            self.size = 0    #长度

    def __len__(self):
        return self.size
    def __str__(self):
        curr = self.head
        data = []
        while curr:
            data.append(curr.data)
            curr = curr.next
        return f'LLQueue({self.size}):{data}'
    def __repr__(self):
        return self.__str__()

    def is_empty(self):
        return self.size == 0

    def add(self, val):
        if not self.tail:
            self.tail = self.LNode(val)
            self.head = self.tail
        else:
            self.tail.next = self.LNode(val)
            self.tail = self.tail.next
        self.size += 1

    def pop(self):
        if (self.is_empty()):
            raise ValueError('Can not dequeue from an empty queue.')
        ret = self.head
        self.head = self.head.next
        ret.next = None
        if not self.head: #head和tail应该都为空
            self.tail = None
        self.size -= 1
        return ret.data
```

##### 2、例：传土豆游戏

```python
#例：每隔固定数目移除一个人，并圆圈循环。
#import Queue
def hotPotato(namelist, num):
    simqueue = Queue()
    for name in namelist:
        simqueue.enqueue(name)
    while simqueue.size() > 1: #把人进行圆圈旋转，每次移除第一个
        for i in range(num):
            simqueue.enqueue(simqueue.dequeue())
        simqueue.dequeue()
    return simqueue.dequeue()

# jj= hotPotato(["Bill", "David", "Susan", "Jane", "Kent", "Brad"], 7)
# print(jj)
```

