import bisect

#二分查找算法
#用于有序list或tuple

L = [1,3,3,4,6,8,12,15]

#查找，存在时返回，不存在时返回应插入位置

#左侧
x = bisect.bisect_left(L, 3)  # 1
#右侧
x = bisect.bisect_right(L, 3)   # 3

#插入

#左侧，若 x 已存在，插入点将位于任何现有条目之前（左侧）
bisect.insort_left(L, 2)
#右侧，若 x 已存在，插入点将位于任何现有条目之后（右侧）
bisect.insort_right(L, 4)