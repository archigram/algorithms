#二分查找（迭代）

def binarySearch(L, item):
    '''迭代实现二分查找，L：有序列表'''
    first = 0
    last = len(L) - 1
    found = False

    while first <= last and not found:
        midpoint = (first + last) // 2
        if L[midpoint] == item:
            found = True
        else:
            if item < L[midpoint]:
                last = midpoint - 1
            else: first = midpoint + 1

    return found

if __name__ == '__main__':
    L1 = [17, 20, 26, 31, 44, 54, 55, 77, 93]
    item = 31
    print(f'{item} 是否在列表中: {binarySearch(L1, item)}')
