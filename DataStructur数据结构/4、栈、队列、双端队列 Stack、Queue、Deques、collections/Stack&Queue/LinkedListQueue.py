
class LinkedListQueue():
    '''
        链表实现队列
    '''
    class LNode: #链表节点
        def __init__(self, data=None, _next=None):
            self.data = data
            self.next = _next

    def __init__(self, lst=None):
        if isinstance(lst, list):  #列表转队列
            self.size = len(lst)
            self.tail = self.LNode(lst[-1])
            head=self.tail
            for x in range(self.size-1):
                N=self.LNode(lst[self.size-2-x],head)
                head=N
            self.head = head
        else:
            self.head = None #头
            self.tail = None #尾
            self.size = 0    #长度

    def __len__(self):
        return self.size
    def __str__(self):
        curr = self.head
        data = []
        while curr:
            data.append(curr.data)
            curr = curr.next
        return f'LLQueue({self.size}):{data}'
    def __repr__(self):
        return self.__str__()

    def is_empty(self):
        return self.size == 0

    def add(self, val):
        if not self.tail:
            self.tail = self.LNode(val)
            self.head = self.tail
        else:
            self.tail.next = self.LNode(val)
            self.tail = self.tail.next
        self.size += 1

    def pop(self):
        if (self.is_empty()):
            raise ValueError('Can not dequeue from an empty queue.')
        ret = self.head
        self.head = self.head.next
        ret.next = None
        if not self.head: #head和tail应该都为空
            self.tail = None
        self.size -= 1
        return ret.data

if __name__ == "__main__":
    l1=LinkedListQueue([1,2,3,4,5,6])
    l2=LinkedListQueue()
    print(l1,l2)

    l2.add(3)
    l1.pop()
    print(l1,l2)