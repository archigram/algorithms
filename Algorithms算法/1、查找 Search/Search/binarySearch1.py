#二分查找（递归）

def binarySearch(L, item):
    '''递归实现二分查找，L：有序列表'''
    if len(L) == 0:
        return False
    else:
        midpoint = len(L) // 2
        if L[midpoint] == item:
            return True
        else:
            if item < L[midpoint]:
                return binary_search(L[:midpoint], item)
            else:
                return binary_search(L[midpoint+1:], item)


if __name__ == '__main__':
    L1 = [17, 20, 26, 31, 44, 54, 55, 77, 93]
    item = 31
    print(f'{item} 是否在列表中: {binarySearch(L1, item)}')