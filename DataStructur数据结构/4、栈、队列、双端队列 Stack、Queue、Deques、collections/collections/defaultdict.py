from collections import defaultdict

#dict子类
#设置一个默认值，索引不存在key时，可以使用默认值

#默认值索引
dict1= defaultdict(lambda: '默认值') #Key不存在时，返回一个默认值
print(dict1["k2"])

#使用默认值list直接添加[]
list2= [('yellow',11),('blue',2),('yellow',3),('blue',4),('red',5),('red',10)]
dict1 = defaultdict(list)
for k,v in list2:
    dict1[k].append(v)
print(dict1)
