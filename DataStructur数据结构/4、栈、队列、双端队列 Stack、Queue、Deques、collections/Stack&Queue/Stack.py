#栈有时也被称作“下推栈”

# Stack()创建一个空栈。它不需要参数，且会返回一个空栈。
# push(item)将一个元素添加到栈的顶端。它需要一个参数item，且无返回值。
# pop()将栈顶端的元素移除。它不需要参数，但会返回顶端的元素，并且修改栈的内容。
# peek()返回栈顶端的元素，但是并不移除该元素。它不需要参数，也不会修改栈的内容。
# isEmpty()检查栈是否为空。它不需要参数，且会返回一个布尔值。
# size()返回栈中元素的数目。它不需要参数，且会返回一个整数。

class Stack:
    '''栈类，用列表实现；list[0]为底端，list[-1]为顶端；添加和移除均在顶端'''
    def __init__(self):
        self.items = []
    def isEmpty(self):
        return self.items == []
    def push(self, item):
        self.items.append(item)
    def pop(self):
        return self.items.pop()
    def peek(self):
        return self.items[len(self.items)-1] #self.items[-1]
    def size(self):
        return len(self.items)
