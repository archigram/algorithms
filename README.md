# 数据结构与算法

#### 介绍

python DataStructur数据结构 Algorithms算法

#### 数据结构-DataStructur

1、数组 Array

2、字符串 Str

3、链表 LinkedList

4、栈、队列、双端队列 Stack、Queue、Deques、collections

5、有序字典、哈希表（散列表） OrderedDict、HashTable

6、二叉树、二叉搜索树、AVL、红黑树 BinaryTree、BST、AVL、RBTree

7、图 Graph

8、堆 Heap

9、并查集 UnionFind



#### 算法-Algorithms

1、查找 Search

2、排序 Sort

3、数论 Math

4、双指针 DualPointer

5、贪心 Greedy

6、动态规划 DP

7、回溯 BackTrace

8、计数、概率、排列组合 Probability

9、建模 Modeling

10、大数据统计 Big Data

