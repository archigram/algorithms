#顺序查找

def sequentialSearch(L, item):
    '''顺序查找，L：无序列表'''
    pos = 0
    found = False  # 标记是否已找到数据项

    while pos < len(L) and not found:
        if L[pos] == item:
            found = True
        else:
            pos += 1
    return found

if __name__ == '__main__':
    L1 = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    item = 31
    print(f'{item} 是否在列表中: {sequentialSearch(L1, item)}')
