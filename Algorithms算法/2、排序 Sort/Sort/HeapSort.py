#堆排序


def heapify(List, start, end): 
    '''start>end'''
    start,end = (start,end) if start>end else (end,start)
    left = 2* end +1 #下标start的节点的左孩子节点的下标
    right = left +1  #下标start的节点的右孩子节点的下标
    maxs = end
    if end < start /2: 
        if left < start and List[left] > List[maxs]: 
            maxs = left
        if right < start and List[right] > List[maxs]: 
            maxs = right
        if maxs != end: 
            List[maxs],List[end] = List[end],List[maxs]
            heapify(List, start, maxs)

def HeapSort(List): 
    size = len(List)
    for i in range(size//2-1, -1, -1): 
        heapify(List, size, i)
    for i in range(size-1, -1, -1): 
        List[0], List[i] = List[i], List[0]
        heapify(List, i, 0)

a=[5,6,7,23,76,9,54,6]
HeapSort(a)
print(a)