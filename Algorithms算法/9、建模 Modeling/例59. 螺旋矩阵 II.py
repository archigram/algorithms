#59. 螺旋矩阵 II
'''
给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。
'''

class Solution:
    def __init__(self):
        self.v = [[1]]
        self.n = 1
    def add_1(self): #从内向外增加圈数
        n = self.n
        if n%2 : #左下
            for i in range(n): self.v[i].insert(0, n**2+i+1)
            self.v.append([i for i in range(n**2+n+1,(n+1)**2+1)])
        else:
            for i in range(n): self.v[i].append(n**2-i+n)
            self.v.insert(0, [2*(n+1)**2-i-n for i in range(n**2+n+1,(n+1)**2+1)])
        self.n += 1
    def generateMatrix(self, n):
        if n-1:
            for i in range(n-1): self.add_1()
        if not n%2 : self.v = [self.v[n-1-i][::-1] for i in range(n)] #矫正从左上起始
        for i in range(n): self.v[i] = [n**2+1-j for j in self.v[i]] #修正从小到大
        print(self.v)
        return self.v

Solution().generateMatrix(5)