# 有序字典

#### 一、collections.OrderedDict

1、相当于双向链表。

2、将类OrderedDict实例化会得到一个dict子类的实例，支持通常的dict方法。

#### 二、数据处理

1、OrderedDict是记住键首次插入顺序的字典。如果新条目覆盖现有条目，则原始插入位置保持不变。

2、删除条目并重新插入会将其移动到末尾。

```python
from collections import OrderedDict

od=OrderedDict()
od['name'] = 'egon'
od['age'] = 18
od['gender'] = 'male'

od['age']=19
print(od) # OrderedDict([('name', 'egon'), ('age', 19), ('gender', 'male')])

del od['age']
od['age']=20
print(od) # OrderedDict([('name', 'egon'), ('gender', 'male'), ('age', 20)])
```

2、方法popitem(last=True)，删除并返回(key, value)对。如果last为真，则以LIFO(后进先出)顺序返回这些键值对，如果为假，则以FIFO(先进先出)顺序返回。

3、方法move_to_end(key, last=True)，将一个已存在的key移动到有序字典的任一端。如果last为True（默认值），则移动到末尾，如果last为False，则移动到开头。

#### 三、数据结构拓展

1、排序字典。OrderedDict与sort结合

```python
# 标准未排序的常规字典
d = {'banana': 3, 'apple': 4, 'pear': 1, 'orange': 2}

# 按照key排序的字典
a=OrderedDict(sorted(d.items(), key=lambda t: t[0]))
#OrderedDict([('apple', 4), ('banana', 3), ('orange', 2), ('pear', 1)])

# 按照value排序的字典
OrderedDict(sorted(d.items(), key=lambda t: t[1]))
#OrderedDict([('pear', 1), ('orange', 2), ('banana', 3), ('apple', 4)])
```

2、条目更新。新条目覆盖现有条目时，将覆盖的条目移动到结尾。

```python
class LastOrdered(OrderedDict):
    def __setitem__(self, key, value):
        if key in self:
            del self[key]
        OrderedDict.__setitem__(self, key, value)

od=LastOrdered()
od['k1']=111
od['k2']=222
od['k3']=333
print(od)
#LastOrdered([('k1', 111), ('k2', 222), ('k3', 333)])

od5['k2']=666
print(od5)
#覆盖的值跑到末尾，LastOrdered([('k1', 111), ('k3', 333), ('k2', 666)])
```

3、顺序统计。OrderDict与collections.Counter结合。

```python
from collections import OrderedDict,Counter

class OrderedCounter(Counter, OrderedDict):
    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, OrderedDict(self))
    def __reduce__(self):
        return self.__class__, (OrderedDict(self),)

od1 = Counter(['bbb','ccc','aaa','aaa','ccc'])
print(od1)
# 顺序错乱：Counter({'ccc': 2, 'aaa': 2, 'bbb': 1})

od2 = OrderedCounter(['bbb','ccc','aaa','aaa','ccc'])
print(od2)
# 顺序保持原有：OrderedCounter(OrderedDict([('bbb', 1), ('ccc', 2), ('aaa', 2)]))
```

