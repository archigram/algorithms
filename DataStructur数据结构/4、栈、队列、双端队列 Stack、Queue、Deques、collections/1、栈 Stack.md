# 栈 Stack

#### 一、内存概念

1、栈有时也被称作“下推栈”。

2、先存进去的数据只能最后被取出来，是 LIFO（Last In First Out，**后进先出**）。

![image-20220924200832201](./assets/image-20220924200832201.png)

#### 二、程序操作

##### 1、创建栈。

```python
# Stack()创建一个空栈。它不需要参数，且会返回一个空栈。
# push(item)将一个元素添加到栈的顶端。它需要一个参数item，且无返回值。
# pop()将栈顶端的元素移除。它不需要参数，但会返回顶端的元素，并且修改栈的内容。
# peek()返回栈顶端的元素，但是并不移除该元素。它不需要参数，也不会修改栈的内容。
# isEmpty()检查栈是否为空。它不需要参数，且会返回一个布尔值。
# size()返回栈中元素的数目。它不需要参数，且会返回一个整数。

class Stack:
    '''栈类，用列表实现；list[0]为底端，list[-1]为顶端；添加和移除均在顶端'''
    def __init__(self):
        self.items = []
    def isEmpty(self):
        return self.items == []
    def push(self, item):
        self.items.append(item)
    def pop(self):
        return self.items.pop()
    def peek(self):
        return self.items[len(self.items)-1] #self.items[-1]
    def size(self):
        return len(self.items)
```

```python
class LinkedListStack(LinkedList):
    '''
        链表实现堆
    '''
    def __init__(self,lst=None):
        super().__init__(lst)
    def __str__(self):
        return f'LLStack({self.size}):{self.list()}'

    def push(self, value):
        self.insert(0, value)
    def pop(self):
        return super().pop(0)
    def peek(self):
        return self[0]
```

