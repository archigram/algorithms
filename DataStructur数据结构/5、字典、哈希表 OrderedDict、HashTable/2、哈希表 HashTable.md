# 哈希 HashTable

#### 一、基本概念

1、用于统计、搜索**特定元素**。

2、哈希表也成散列表。hash算法是把数据通过函数映射成唯一数值，不同数据映射的哈希值长度相同。

3、python内置hash函数。

#### 二、数据处理

#### 1、基本数据处理

![image-20220924205206378](./assets/image-20220924205206378.png)

哈希表通常直接用**元组（列表）、集合、字典**来表示。

```python
#哈希表类（加1法）
class HashTable:
    '''取余法创建散列表，线性探测法解决冲突（加1法）'''
    def __init__(self,size=10):
        self.size = size
        self.slots = [None] * self.size #槽键
        self.data = [None] * self.size #槽值
    
    def _hash(self, key, size): #取余法
        return (hash(key) & 0x7fffffff) % self.size #key%self.size
    
    def rehash(self, oldhash, size): #加1法
        return (oldhash + 1)%size

    def __setitem__(self, key, data):
        val = self._hash(key, self.size)
        if self.slots[val] == None:
            self.slots[val] = key
            self.data[val] = data
        else:
            if self.slots[val] == key: self.data[val] = data #替换
            else:
                slot = self.rehash(val, self.size)
                while self.slots[slot] and  (self.slots[slot] != key):
                    slot = self.rehash(slot, self.size)
                if self.slots[slot] == None:
                    self.slots[slot] = key
                    self.data[slot] = data
                else: self.data[slot] = data #替换

    def __getitem__(self, key):
        start = self._hash(key, self.size)
        data = None
        stop = False
        found = False
        position = start
        while self.slots[position] and not(found or stop):
            if self.slots[position] == key:
                found = True
                data = self.data[position]
            else:
                position=self.rehash(position, self.size)
                if position == start: stop = True
        return data
```

#### 2、哈希碰撞

##### (1) 链接法

把碰撞的元素以链表方式存储在同一个位置。

##### (2) 线性探测法

从碰撞位置开始往后依次（或特定步长）寻找哈希表中的空位。

##### 3、例：散列搜索算法
