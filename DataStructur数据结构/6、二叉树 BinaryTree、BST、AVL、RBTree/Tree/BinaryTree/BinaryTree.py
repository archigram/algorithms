#二叉树
#节点与引用（面向对象）

class BinaryTree:
    '''二叉树类'''
    def __init__(self, tree, left = None, right = None):
        self.val = tree
        self.left = left
        self.right = right
    def insertLeft(self, newNode):
        if self.left == None:
            self.left = BinaryTree(newNode)
        else:   #若左子树不为空，则需要下移的插入子树的左枝
            t = BinaryTree(newNode)
            t.left = self.left
            self.left = t
    def insertRight(self, newNode):
        if self.right == None:
            self.right = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.right = self.right
            self.right = t
    def getRightChild(self):
        return self.right
    def getLeftChild(self):
        return self.left
    def setRootVal(self, obj):
        self.val = obj
    def getRootVal(self):
        return self.val
    def __str__(self): #中序遍历打印
        result = []
        def inorder(tree):
            if tree:
                inorder(tree.left)
                result.append(tree.val)
                inorder(tree.right)
        inorder(self)
        return 'tree:'+str(result)
    def __eq__(self, tree2): #中序遍历判断
        if (not self) and (not tree2): return True
        if self and (not tree2): return False
        if (not self) and tree2 : return False
        if self.val == tree2.val :
            return self.left == tree2.left and \
                self.right == tree2.right
        else : return False

#tree = BinaryTree(0,BinaryTree(1,BinaryTree(3),BinaryTree(5)),BinaryTree(2,BinaryTree(4),BinaryTree(6)))
