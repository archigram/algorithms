import heapq

#添加元素。
heapq.heappush(heap:list, item:float)
'''
data = [97, 38, 27, 50, 76, 65, 49, 13]
heap = []
for n in data:
heapq.heappush(heap, n)'''

#列表堆化重排
heapq.heapify(List:list)
'''
data = [97, 38, 27, 50, 76, 65, 49, 13]
heapq.heapify(data)'''

#删除并返回堆顶元素，即heap中最小的元素。
heapq.heappop(heap:list)
#替换并返回堆顶元素。
heapreplace(heap:list, item:float)
#添加元素，并返回重新堆化后的堆顶元素。
heapq.heappushpop(heap:list, item:float)

#merge是多路归并排序。
heapq.merge(heap:list[,heap:list [,heap:list [...]]])

#获取最大或者最小的n个数。
heapq.nlargest(n, iterable, key=None)
#结果上等价于sorted(iterable, key=key, reverse=True)[:n]
heapq.nsmallest(n, iterable, key=None)
#结果上等价于sorted(iterable, key=key)[:n]