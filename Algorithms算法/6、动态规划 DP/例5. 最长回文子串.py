##5. 最长回文子串
'''
给你一个字符串 s，找到 s 中最长的回文子串。
'''

#动态规划：简称DP，如果某一问题有很多重叠子问题，使用动态规划是最有效的。
# 1、确定dp数组（dp table）以及下标的含义
# 2、确定递推公式
# 3、dp数组如何初始化
# 4、确定遍历顺序
# 5、举例推导dp数组

