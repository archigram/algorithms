#无序列表：元素没有按顺序排列的列表。列表元素位置是固定的

class Node:
    '''链表节点'''
    def __init__(self, initdata):
        self.data = initdata
        self.next = None
    def setData(self, newdata):
        self.data = newdata
    def setNext(self, newnext):
        self.next = newnext

class UnorderList:
    '''链表实现无序列表，时间复杂度为O(n)'''
    def __init__(self):
        self.head = None
    def ise(self):
        return self.head == None
    def add(self,item):
        temp = Node(item)
        temp.setNext(self.head) #链接
        self.head = temp
    def length(self):
        current = self.head
        count = 0
        while current != None:
            count += 1
            current = current.next
        return count
    def search(self, item):
        current = self.head
        found = False
        while current != None and not found:
            if current.data == item : found = True
            else : current = current.next
        return found
    def remove(self, item):
        current,previous = self.head,None
        found = False
        while not found:
            if current.data == item : found = True
            else : previous,current = current,current.next
        if previous == None : self.head = current.next
        else: previous.next = current.next


p = UnorderList()
p.add('66')
p.add(8)
p.add([9])
# print(p.length())
# print(p.search(8))
# p.remove('66')
# print(p.length())
# print(p.search('66'))
# print(p.head.data)
