## 1. 两数之和
'''
给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
**示例:**
给定 nums = [2, 7, 11, 15], target = 9
因为 nums[0] + nums[1] = 2 + 7 = 9
所以返回 [0, 1]
'''
class Solution(object):
    def twoSum(self, nums, target):
        mapping = {}
        for index, val in enumerate(nums):
            diff = target - val
            if diff in mapping:
                return [mapping[diff], index]
            else:
                mapping[val] = index

#暴力的解法是两层for循环查找，时间复杂度是O(n^2)。
#**什么时候使用哈希法**，当我们需要查询一个元素是否出现过，或者一个元素是否在集合{}[]里的时候，就要第一时间想到哈希法。 