
class LinkedListStack(LinkedList):
    '''
        链表实现堆
    '''
    def __init__(self,lst=None):
        super().__init__(lst)
    def __str__(self):
        return f'LLStack({self.size}):{self.list()}'

    def push(self, value):
        self.insert(0, value)
    def pop(self):
        return super().pop(0)
    def peek(self):
        return self[0]

if __name__ == '__main__':
    ll=LinkedListStack([i for i in range(10)])
    ll.push(67)
    l1=ll.pop()
    l2=ll.peek()
    print(ll,l1,l2)
