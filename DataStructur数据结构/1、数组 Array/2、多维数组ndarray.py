import numpy as np


##创建array


#从tuple、list、array转化
#numpy.array(object, dtype=None, copy=True, order='K', subok=False, ndmin=0)

a = np.array([(1.5,2,3),
              (4, 5, 6)]) #从元组转化
b = np.array([[1,2],[3,4]], dtype=complex) #创建复数数组
#[[1.+0.j 2.+0.j],[3.+0.j 4.+0.j]]
c = np.array([1, 2., 3.], ndmin=2) #最小维度
#[[1. 2. 3.]]
d = np.array(np.mat('1 2;3 4')) #从子类创建
#[[1 2],[3 4]]
e = np.array(np.mat('1 2;3 4'), subok=True) #保持子类类型
#matrix([[1, 2], [3, 4]])


#生成均匀分布的array

f = np.arange(15)
#[ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14]
g = np.arange(15).reshape(3,5)
# [[ 0  1  2  3  4]
#  [ 5  6  7  8  9]
#  [10 11 12 13 14]]
h = np.arange( 0, 2, 0.3 ) #按照步长
#array([ 0. ,  0.3,  0.6,  0.9,  1.2,  1.5,  1.8])

#np.linspace(start, stop, num, endpoint, retstep, dtype) #等差数列
i = np.linspace(1,3,9)
#[ 1.  1.25  1.5   1.75  2.  2.25  2.5   2.75  3.  ]
j = np.linspace(1,3,8,endpoint=False,retstep=True) #不包括截止数值，retstep=True：展示出步长
#(array([1. , 1.25, 1.5 , 1.75, 2.  , 2.25, 2.5 , 2.75]), 0.25)

#np.logspace(start, stop, num=num, endpoint=endpoint, base=10, dtype=dtype) #等比数列
k = np.logspace(1,3,num=4) #默认以10为底，输出4个 10~10^3的数
#[ 10.  46.41588834  215.443469   1000. ]
l = np.logspace(1,4,num=3,endpoint=False,base=2) #2^1~2^4，不包括2^4
#[2. 4. 8.]


#六个特定数组

m = np.ones((3,4)) #创建元素全为 1 的数组
n = np.zeros((3,4)) #创建元素全为 0 的数组
o = np.full((3,4),2) #创建元素全为 2 的数组

p = np.empty((3,4)) #创建一个内容随机并且依赖与内存状态的数组

q = np.eye(3,4) #创建一个对角线为 1 其他为 0 的矩阵
r = np.identity(3) #创建一个主对角线为 1 其他为 0 的方阵


#数组转化

s = np.ones_like(q) #根据 q 形状创建元素全为 1 的数组。
#还有zeros_like、full_like

t = np.concatenate((m, n),axis=0) #合并数组
#axis=0维持列数（2x2+2x2 --> 4x2），axis=1维持行数（2x2+2x2 --> 2x4）。
#np.vstack((m,n)) # axis=0
#np.hstack((m,n)) # axis=1

u = np.fromfunction(lambda i,j: (i+1)*(j+1), (9, 9), dtype=int) #打印乘法表
#lambda 可以换成其他函数，参数个数为矩阵维度。

#数组属性

a.ndim   #数组的维数
a.shape  #数组每一维的大小
a.size   #数组全部元素的数量 
a.dtype  #数组中元素的类型 #float64
a.itemsize  #每个元素所占的字节数


#索引，切片，赋值

arr = np.array([(1, 2, 3),
                (4, 5, 6),
                (7, 8, 9)])
a = arr[2,1] #行列分别索引、切片
#8
b = arr[0,:] #等价于arr[0,...]或arr[0] #行列分别索引、切片
#[1 2 3]
arr[1,:] = [0,0,10] #对第二行赋值
#[[ 1  2  3]
# [ 0  0 10]
# [ 7  8  9]]
c = arr[...,1] #第二列
d = arr[...,1:] #第一列之后所有列


#数学运算

a+b #按位求和
a-b #按位求差
a>2 #按位比较，返回bull矩阵
a*2 #按位数乘
a*b #按位求积
a/b #按位求商
a.dot(b) #矩阵乘法
c = a.copy() #深拷贝
a.sum() #求和
a.sum(axis=0) #列求和，返回行矩阵
a.min() #求最小值
a.max() #求最大值
np.sin(a) #按位求sin
np.floor(a) #按位取整
np.exp(a) #按位求e^x
a.transpose() #转置
a.T #转置


#数据操作

np.reshape(arr,shape) #按新形状重排，不改变原数组
arr.reshape(shape) #按新形状重排，不改变原数组
arr.resize(shape) #按新形状重排，改变原数组
np.swapaxes(arr, axis1, axis2) #交换维度，用于多维
np.tile(A, reps) #A复制reps次，优先是沿着行方向复制
arr.flatten(order) #拍平，'C'为行拍平，'F'为列拍平
np.expand_dims(arr, axis) #增加维度
np. squeeze(arr, axis=None) #将arr中长度为1的轴移除

np.fliplr(arr) #反转列，left-right
np.flipud(arr) #反转行，up-down
np.delete(arr,obj,axis) #删除行或列，若axis=None则拍平删除
np.insert(arr,obj,values,axis) #添加行或列
np.append(arr,values,axis) #末尾添加
np.split(arr,spl,axis) #spl为整数或数组，为数组则拆成多段
np.stack((a,b,...),axis) #拼接

np.sort(arr,axis,order) #排序，可以指定规则
np.argsort(arr) #升序，返回索引;argsort(arr)降序

arr.astype(new_type) #修改元素类型
#arr.astype(np.float32)

arr.tolist() #转list
