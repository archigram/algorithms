#二叉树
#列表之列表（函数方法）

def BinaryTree(r):
    return [r, [], []]

def insertLeft(root, newBranch): #插入左子树
    t = root.pop(1)
    if len(t) > 1: #若左子树不为空，则需要下移的插入子树的左枝
        root.insert(1, [newBranch, t, []])
    else:
        root.insert(1, [newBranch, [], []])
    return root
def insertRight(root, newBranch): #插入左子树
    t = root.pop(2)
    if len(t) > 1:
        root.insert(2, [newBranch, [], t])
    else:
        root.insert(2, [newBranch, [], []])
    return root

def getRootVal(root):
    return root[0]
    
def setRootVal(root, newVal):
    root[0] = newVal

def getLeftChild(root):
    return root[1]
    
def getRightChild(root):
    return root[2]