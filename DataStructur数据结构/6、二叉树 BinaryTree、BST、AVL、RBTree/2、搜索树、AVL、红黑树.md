# 二叉树-搜索树、AVL、红黑树 BST、AVL、BR

#### 一、基本概念

##### 1、二叉搜索树。

(1) 二叉搜索树（BST），又称二叉**排序树**、二叉**查找树**。

(2) 二叉排序树**中序遍历**结果为一个**递增**数列（非严格递增）。二叉排序树是二分法的搜索原理。

![image-20220927154740616](./assets/image-20220927154740616.png)

##### 2、平衡二叉搜索树。

平衡二叉搜索树（AVL），是一棵空树或它的左右两个子树的高度差的绝对值不超过1，并且左右两个子树都是一棵平衡二叉树。

##### 3、红黑树。

红黑树是一种自平衡二叉搜索树。

#### 二、属性

##### 1、查找元素。

```python
#递归法
def searchBST(root, val):
    if not root or root.val == val: 
        return root
    if root.val > val: 
        return self.searchBST(root.left, val)
    if root.val < val: 
        return self.searchBST(root.right, val)
```

```python
#迭代法
def searchBST(root, val):
    while root:
        if val < root.val: root = root.left
        elif val > root.val: root = root.right
        else: return root
    return None
```

##### 2、有效性验证。

验证一个树是否为二叉搜索树。

```python
#方法一：中序遍历为数组进行验证。
```

```python
#方法二
#递归法
def isValidBST(root):
    cur_max = -float('inf')
    def isBST(root): 
        nonlocal cur_max #用于修改外层函数变量
        if not root: 
            return True
        is_left_valid = isBST(root.left)
        if cur_max < root.val: 
            cur_max = root.val
        else: return False
        is_right_valid = isBST(root.right)
        return is_left_valid and is_right_valid
    return isBST(root)
```

```python
#方法二
#迭代法
def isValidBST(root):
    stack = []
    cur = root
    pre = None
    while cur or stack:
        if cur:
            stack.append(cur)
            cur = cur.left
        else: # 逐一处理节点
            cur = stack.pop()
            if pre and cur.val <= pre.val:
                return False
            pre = cur 
            cur = cur.right
    return True
```

##### 3、二叉搜索树的众数。

```python
def findMode(root):
    stack = []
    cur = root
    pre = None
    maxCount, count = 0, 0
    res = []
    while cur or stack:
        if cur:
            stack.append(cur)
            cur = cur.left
        else:  # 逐一处理节点
            cur = stack.pop()
            if pre == None:  # 第一个节点
                count = 1
            elif pre.val == cur.val:
                count += 1
            else:
                count = 1
            if count == maxCount:
                res.append(cur.val)
            if count > maxCount:
                maxCount = count
                res.clear()
                res.append(cur.val)
            pre = cur
            cur = cur.right
    return res  
```

##### 4、最近公共祖先。

```python
#递归法
def lowestCommonAncestor(root: 'TreeNode', p: 'TreeNode', q: 'TreeNode'):
    if root.val > p.val and root.val > q.val:
        return self.lowestCommonAncestor(root.left, p, q)
    if root.val < p.val and root.val < q.val:
        return self.lowestCommonAncestor(root.right, p, q)
    return root
```

```python
#迭代法
def lowestCommonAncestor(root: 'TreeNode', p: 'TreeNode', q: 'TreeNode'):
    while True:
        if root.val > p.val and root.val > q.val:
            root = root.left
        elif root.val < p.val and root.val < q.val:
            root = root.right
        else:
            return root
```

#### 三、修改与构造

##### 1、插入元素。

```python
#递归法 - 有返回值
def insertIntoBST(root, val):
    if not root: return BinaryTree(val)
    if val < root.val: 
        root.left = insertIntoBST(root.left, val)
    if root.val < val:
        root.right = insertIntoBST(root.right, val)
    return root
```

```python
#递归法 - 无返回值
def insertIntoBST(root, val):
    newNode = BinaryTree(val)
    if not root: return newNode 
    if not root.left and val < root.val:
        root.left = newNode
    if not root.right and val > root.val:
        root.right = newNode 
    if val < root.val:
        insertIntoBST(root.left, val)
    if val > root.val:
        insertIntoBST(root.right, val)
```

##### 2、创建有索引的二叉搜索树。

##### 3、创建有索引的AVL树。

##### 4、创建有索引的RBTree。

