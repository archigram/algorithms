from collections import namedtuple

#tuple子类
#子类拥有名称
#继承元组不可变特性


#子类：元组名称(子类类型)、元组内每个元素的名称(属性)、子类的长度len()
vector=namedtuple('vector',['x','y','z'])

#创建子类对象 
flag=vector(3,4,5)

#类型验证
print(isinstance(flag,vector)) #True
print(isinstance(flag,tuple)) #True

#调用对象，按照属性名称调用，每个对象。
print(flag.x,flag.y,flag.z)