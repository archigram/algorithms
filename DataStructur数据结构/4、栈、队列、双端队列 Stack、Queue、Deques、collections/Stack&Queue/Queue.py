#队列 就像排队，队列是有序集合，添加操作发生在“尾部”，移除操作则发生在“头部”。
#这种排序原则被称作FIFO（first-in first-out），即先进先出，也称先到先得。

# Queue()创建一个空队列。它不需要参数，且会返回一个空队列。
# enqueue(item)在队列的尾部添加一个元素。它需要一个元素作为参数，不返回任何值。
# dequeue()从队列的头部移除一个元素。它不需要参数，且会返回一个元素，并修改队列的内容。
# isEmpty()检查队列是否为空。它不需要参数，且会返回一个布尔值。
# size()返回队列中元素的数目。它不需要参数，且会返回一个整数。

class Queue:
    '''队列类，用列表实现；list[0]为头部，list[-1]为尾部；添加在头部，移除在尾部'''
    def __init__(self):
        self.items = []
    def isEmpty(self):
        return self.items == []
    def enqueue(self, item):
        self.items.insert(0, item)
    def dequeue(self):
        return self.items.pop()
    def size(self):
        return len(self.items)
