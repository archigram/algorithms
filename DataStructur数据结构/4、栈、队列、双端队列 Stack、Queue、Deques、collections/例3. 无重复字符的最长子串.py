##3. 无重复字符的最长子串
'''
给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
'''

class Solution(object):
    def lengthOfLongestSubstring(self, s):
        mapSet = {}
        start, result = 0, 0
        for end in range(len(s)):
            if s[end] in mapSet:
                start = max(mapSet[s[end]], start)
            result = max(result, end-start+1)
            mapSet[s[end]] = end+1
        return result 