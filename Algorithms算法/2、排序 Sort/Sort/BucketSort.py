#桶排序


def BucketSort(nums):
    minv, maxv = min(nums), max(nums)
    countn = [0]*(maxv-minv+1)
    for i in nums:
        countn[i-minv] += 1
    n = 0
    for i in range(len(countn)):
        p=countn[i]
        while countn[i] and p:
            nums[n]=i+minv
            n += 1; p-=1


if __name__ == "__main__":
    a = [10, 19, 82, 74, 6, 5, 3, 3, 2, 1]
    BucketSort(a)
    print(a)
