#leetcode 82. 删除排序链表中的重复元素 II
'''
给定一个已排序的链表的头 head ， 删除原始链表中所有重复数字的节点，只留下不同的数字 。返回 已排序的链表 。
输入：head = [1,2,3,3,4,4,5]
输出：[1,2,5]
'''

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if (not head) or (not head.next): return head
        dummy = ListNode(0, head)
        cur = dummy
        while cur.next and cur.next.next:
            if cur.next.val == cur.next.next.val: #查找相同值
                x = cur.next.val                  #记录相同值
                while cur.next and cur.next.val == x: #迭代删除
                    cur.next = cur.next.next
            else:
                cur = cur.next
        return dummy.next