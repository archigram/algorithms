from collections import Counter

#counter可以支持方便、快速的计数
#计数结果降序排列

#字符串计数
str1="abcdefgabcedergeghdjlkabcdefe" #将可迭代的字符串初始化counter
str2=Counter(str1)
print(str2) #从输出的内容来看，Counter实际上也是dict的一个子类
#Counter({'e': 6, 'd': 4, 'a': 3, 'b': 3, 'c': 3, 'g': 3, 'f': 2, 'r': 1, 'h': 1, 'j': 1, 'l': 1, 'k': 1})

#dict计数
dict3 = {"banana": 33, "apple": 222, "pear": 1, "orange": 4444,"apples":2}
dict4=Counter(dict3)
print(dict4)
#Counter({'orange': 4444, 'apple': 222, 'banana': 33, 'apples': 2, 'pear': 1})
print(dict4["test"])
#key缺失，会返回0，而不是报出KeyError；

#截取计数最多的三组值
list1 = str2.most_common(3) #[('e', 6), ('d', 4), ('a', 3)]
print(list1)

#elements返回一个迭代器,按照数目打印
list2=list(str2.elements())
print(list2)
#['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c', 'd', 'd', 'd', 'd', 'e', 'e', 'e', 'e', 'e', ...]
