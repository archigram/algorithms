import itertools

'''
无穷迭代器
    count(start[,step]) #无限增大的迭代器
    cycle()             #把传入的可迭代对象循环迭代
    repeat(elem[,n])    #单个元素重复迭代，n为次数，不传入为无限迭代
有限迭代器
    chain(p,q…)         #可以把几个迭代器合并成一个并迭代
    chain.from_iterable() #可以把一个迭代器内的多个迭代器合并成一个并迭代 [[...],[...]]
    groupby(iterable[,key=None]) #相邻元素分组迭代，可以按照key函数分组。见下面
    accumulate(iterable [,func]) #根据函数计算结构返回迭代器，默认为迭代求和函数。见下面
    starmap()           #迭代函数结构 starmap(pow, [(2,5), (3,2), (10,3)]) --> 32 9 1000
    compress()          #索引迭代 compress('ABCDEF', [1,0,1,0,1,1]) --> A C E F
    islice()            #切片迭代 islice('ABCDEFG', 2, None) --> C D E F G
    dropwhile()         #条件迭代，当返回False开始迭代 dropwhile(lambda x: x<5, [1,4,6,4,1]) --> 6 4 1
    takewhile()         #条件迭代，当返回False停止迭代 dropwhile(lambda x: x<5, [1,4,6,4,1]) --> 1 4
    filterfalse()       #条件迭代，只返回False的对象 filterfalse(lambda x: x%2, range(10)) --> 0 2 4 6 8
    pairwise()          #每次迭代两个对象，并重叠迭代 pairwise('ABCDEFG') --> AB BC CD DE EF FG
    zip_longest()       #组合迭代。zip_longest('ABC','xy',fillvalue='-') --> ('A','x'),('B','y'),('C','-')
    tee(iterable,n=2)   #可以把一个迭代器复制为n个迭代器
组合迭代器
    product(*iterables, repeat=1)   #返回输入迭代器的笛卡尔积，即迭代器组成的矩阵。见下面
    permutations(iterable, r=None)  #返回迭代对象的全排列，r为单个排列元素数量。
    combinations(iterable, r)       #返回迭代对象的全组合，r为单个组合元素数量。
    combinations_with_replacement(iterable, r) #全组合，允许自己和自己组合。
'''

for i,group in itertools.groupby('aaabbbcccdddaahhff'):
    print(list(group))
'''
['a', 'a', 'a']
['b', 'b', 'b']
['c', 'c', 'c']
['d', 'd', 'd']
['a', 'a']
['h', 'h']
['f', 'f']
'''

for i in itertools.accumulate(range(10)):
    print(i)
#0,1,3,6,10,15,21,28,36,45

for i in itertools.accumulate([0,1,0,2,5,4,6,3], max):
    print(i)
#0,1,1,2,5,5,6,6


for i in itertools.product('AB', range(3)):
    print(i)
# ('A',0) ('A',1) ('A',2) ('B',0) ('B',1) ('B',2)

for i in itertools.product('AB', repeat=2):
    print(i)
#('A', 'A'),('A', 'B'),('B', 'A'),('B', 'B')

for i in itertools.product('AB','abc'):
    print(''.join(i))
#Aa,Ab,Ac,Ba,Bb,Bc