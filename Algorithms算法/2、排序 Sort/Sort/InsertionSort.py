#插入排序

def InsertionSort(List):
    for i in range(1, len(List)):
        temp = List[i]
        j = i
        while j >= 1 and List[j-1] > temp:
            List[j] = List[j-1]
            j -= 1
        List[j] = temp

# a=[7,8,6,9,5,3,0,3]
# InsertionSort(a)
# print(a)
