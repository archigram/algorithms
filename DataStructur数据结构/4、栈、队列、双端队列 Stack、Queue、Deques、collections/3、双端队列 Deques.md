# 双端队列 Deques

#### 一、内存概念

队列和栈的结合，有前端和后端两个数据推入和取出端口。

![image-20220924202703352](./assets/image-20220924202703352.png)

#### 二、程序操作

##### 1、自带双端队列模块。

```python
from collections import deque

#双端队列
#可以近似O(1)的性能在两端插入和删除元素

list1=[x*x for x in range(10)]
delist=deque(list1) #对列表进行了一次再处理，让list1列表变成了双向链表结构
delist.append(1000)#将x添加到deque的右侧
delist.appendleft(2000)#将x添加到deque的左侧
delist.pop()#移除和返回deque中最右侧的元素，如果没有元素，将会报出IndexError；
delist.popleft()#移除和返回deque中最左侧的元素，如果没有元素，将会报出IndexError；
delist.count(1)#返回deque中元素等于1的个数
delist.remove(81)#移除第一次出现的value，如果没有找到，报出ValueError；
delist.reverse()#反转deque中的元素，并返回None；
list2=[1,3,4,5]
delist.extend(list2)#将可迭代变量iterable中的元素添加至deque的右侧
#[64, 49, 36, 25, 16, 9, 4, 1, 0, 1, 3, 4, 5] delist+list2
delist.extendleft(list2)#将变量iterable中的元素添加至deque的左侧，注意 list2反向
#[5, 4, 3, 1, 64, 49, 36, 25, 16, 9, 4, 1, 0] reverse(list2)+delist
delist.rotate(4)#从右侧反转n步到左侧，如果n为负数，则从左侧反转
#[1, 3, 4, 5, 64, 49, 36, 25, 16, 9, 4, 1, 0] 即：delist[-4:]+delist[:-4]
delist.clear()#将deque中的元素全部删除，最后长度为0；
```

##### 2、创建双端队列。

```python
# Deque()创建一个空的双端队列。它不需要参数，且会返回一个空的双端队列。
# addFront(item)将一个元素添加到双端队列的前端。它接受一个元素作为参数，没有返回值。
# addRear(item)将一个元素添加到双端队列的后端。它接受一个元素作为参数，没有返回值。
# removeFront()从双端队列的前端移除一个元素。它不需要参数，且会返回一个元素，并修改双端队列的内容。
# removeRear()从双端队列的后端移除一个元素。它不需要参数，且会返回一个元素，并修改双端队列的内容。
# isEmpty()检查双端队列是否为空。它不需要参数，且会返回一个布尔值。
# size()返回双端队列中元素的数目。它不需要参数，且会返回一个整数。

class Deque:
    '''双端队列类，用列表实现；list[0]为头部，list[-1]为尾部'''
    def __init__(self):
        self.items = []
    def isEmpty(self):
        return self.items == []
    def addFront(self, item):
        self.items.append(item)
    def addRear(self, item):
        self.items.insert(0, item)
    def removeFront(self):
        return self.items.pop()
    def removeRear(self):
        return self.items.pop(0)
    def size(self):
        return len(self.items)
```

