# 查找 Search

#### 1、顺序搜索  BubbleSort

![image-20220929090704104](./assets/image-20220929090704104.png)

##### (1) 有序列表

##### (2) 无序列表

- 查找目标值
- 搜索最大（小）值

#### 2、二分查找 binarySearch

![image-20220929092342489](./assets/image-20220929092342489.png)

##### (1) 有序列表

- 递归
- 迭代

##### (2) 二叉搜索树

- 递归
- 迭代

##### (3) bisect模块函数

```python
import bisect

#二分查找算法
#用于有序list或tuple

L = [1,3,3,4,6,8,12,15]

#查找，存在时返回，不存在时返回应插入位置
x = bisect.bisect_left(L, 3)  #左侧 1
x = bisect.bisect_right(L, 3)  #右侧 3

#插入，若 x 已存在，插入点将位于任何现有条目之前（之后）

bisect.insort_left(L, 2)
bisect.insort_right(L, 4)
```

#### 3、哈希表 HashTable

![image-20220929092522948](./assets/image-20220929092522948.png)

#### 4、树(图)数据查找

##### (1) 深度优先算法 DFS

##### (2) 广度优先算法 BFS

