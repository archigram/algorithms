from collections import deque

#双端队列
#可以近似O(1)的性能在两端插入和删除元素

list1=[x*x for x in range(10)]
delist=deque(list1) #对列表进行了一次再处理，让list1列表变成了双向链表结构
delist.append(1000)#将x添加到deque的右侧
delist.appendleft(2000)#将x添加到deque的左侧
delist.pop()#移除和返回deque中最右侧的元素，如果没有元素，将会报出IndexError；
delist.popleft()#移除和返回deque中最左侧的元素，如果没有元素，将会报出IndexError；
delist.count(1)#返回deque中元素等于1的个数
delist.remove(81)#移除第一次出现的value，如果没有找到，报出ValueError；
delist.reverse()#反转deque中的元素，并返回None；
list2=[1,3,4,5]
delist.extend(list2)#将可迭代变量iterable中的元素添加至deque的右侧
#[64, 49, 36, 25, 16, 9, 4, 1, 0, 1, 3, 4, 5] delist+list2
delist.extendleft(list2)#将变量iterable中的元素添加至deque的左侧，注意 list2反向
#[5, 4, 3, 1, 64, 49, 36, 25, 16, 9, 4, 1, 0] reverse(list2)+delist
delist.rotate(4)#从右侧反转n步到左侧，如果n为负数，则从左侧反转
#[1, 3, 4, 5, 64, 49, 36, 25, 16, 9, 4, 1, 0] 即：delist[-4:]+delist[:-4]
delist.clear()#将deque中的元素全部删除，最后长度为0；
