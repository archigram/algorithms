#稠密图

class DenseGraph:
    def __init__(self, n, directed=False):
        self.n = n  #顶点vertex
        self.m = 0  #边edge
        self.directed = directed #有向
        self.martix = [[0]*n for i in range(n)] #邻接矩阵

    def __str__(self):
        for line in self.martix:
            print(str(line))
        return ''

    def __iter__(self):
        return iter(range(self.m))

    def __getitem__(self, vs):
        if isinstance(vs,tuple) and len(vs)==2:
            return self.martix[vs[0]][vs[1]]
        elif isinstance(vs,int):
            return iter([(vs,i,j) for i,j in \
                enumerate(self.martix[vs]) if j])
        else : return

    def __setitem__(self, vs, w):
        if not isinstance(vs,tuple) and len(vs) != 2:
            raise Exception('Two Vertexs needed')
        v1,v2 = vs
        if 0 <= v1 < self.n and 0 <= v2 < self.n:
            if not self.martix[v1][v2] :
                self.m += 1
            self.martix[v1][v2] = w
            if v1 != v2 and not self.directed :
                self.martix[v2][v1] = w 
        else: raise Exception('Vertex not in the graph')


g=DenseGraph(5)
g[0,3]=43
g[0,4]=32
for i in g[0]:
    print(i)
print(g[3,0])