#双端队列 对在哪一端添加和移除元素没有任何限制。添加或删除既可以在前端，也可以在后端。

# Deque()创建一个空的双端队列。它不需要参数，且会返回一个空的双端队列。
# addFront(item)将一个元素添加到双端队列的前端。它接受一个元素作为参数，没有返回值。
# addRear(item)将一个元素添加到双端队列的后端。它接受一个元素作为参数，没有返回值。
# removeFront()从双端队列的前端移除一个元素。它不需要参数，且会返回一个元素，并修改双端队列的内容。
# removeRear()从双端队列的后端移除一个元素。它不需要参数，且会返回一个元素，并修改双端队列的内容。
# isEmpty()检查双端队列是否为空。它不需要参数，且会返回一个布尔值。
# size()返回双端队列中元素的数目。它不需要参数，且会返回一个整数。

class Deque:
    '''双端队列类，用列表实现；list[0]为头部，list[-1]为尾部'''
    def __init__(self):
        self.items = []
    def isEmpty(self):
        return self.items == []
    def addFront(self, item):
        self.items.append(item)
    def addRear(self, item):
        self.items.insert(0, item)
    def removeFront(self):
        return self.items.pop()
    def removeRear(self):
        return self.items.pop(0)
    def size(self):
        return len(self.items)
