#选择排序

def SelectionSort(List):
    for i in range(len(List)-1, 0, -1):
        Max = 0
        for j in range(i):
            if List[j+1] > List[Max]:
                Max = j+1
        List[i], List[Max] = List[Max], List[i]


# a=[7,8,6,9,5,3,0,3]
# SelectionSort(a)
# print(a)
