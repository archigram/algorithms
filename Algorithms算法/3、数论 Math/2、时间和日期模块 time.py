import time

#时间戳，1970年-2038年
ticks = time.time()

#当前时间：返回时间元组
localtime = time.localtime()
#年，月，日，时，分，秒，星期(0-6，0是周一)，本年度天数(1-366)，夏令时
#tm_year=2022, tm_mon=9, tm_mday=30, tm_hour=12, tm_min=11, tm_sec=41, \
#tm_wday=4, tm_yday=273, tm_isdst=0

#时间元组转时间戳
t = (2022, 9, 30, 12, 11, 41, 4, 273, 0)
secs = time.mktime(t)

#时间格式化
#格式化成2022-09-30 12:17:47形式
loctime1 = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
#格式化成Fri Sep 30 12:17:47 2022 形式
loctime2 = time.strftime("%a %b %d %H:%M:%S %Y", time.localtime())
#格式化成Fri Sep 30 12:17:47 2022 形式
loctime2 = time.asctime(time.localtime())
#格式化成Fri Sep 30 12:17:47 2022 形式
loctime2 = time.ctime()

# 将格式字符串转换为时间戳
a = "2022-09-30 12:17:47"
tim1 = time.mktime(time.strptime(a,"%Y-%m-%d %H:%M:%S"))
'''
%y 两位数的年份表示（00-99）
%Y 四位数的年份表示（000-9999）
%m 月份（01-12）
%d 月内中的一天（0-31）
%H 24小时制小时数（0-23）
%I 12小时制小时数（01-12）
%M 分钟数（00=59）
%S 秒（00-59）
%a 本地简化星期名称
%A 本地完整星期名称
%b 本地简化的月份名称
%B 本地完整的月份名称
%c 本地相应的日期表示和时间表示
%j 年内的一天（001-366）
%p 本地A.M.或P.M.的等价符
%U 一年中的星期数（00-53）星期天为星期的开始
%w 星期（0-6），星期天为星期的开始
%W 一年中的星期数（00-53）星期一为星期的开始
%x 本地相应的日期表示
%X 本地相应的时间表示
%Z 当前时区的名称
%% %号本身
'''

#使程序延迟秒数
#time.sleep(secs)


from datetime import datetime,timedelta

#当前时间类
now = datetime.now() #2022-09-30 12:42:24.990769
y = now.year   #2022
m = now.month
d = now.day
h = now.hour
m = now.minute
s = now.second
ms = now.microsecond #微秒990769

#时间间隔
# 271 days, 19:00:00
delta1 = datetime(2022, 9, 30, 20) - datetime(2022, 1, 2, 1)

#时间差对象
#timedelta(days[,seconds[,microseconds[,milliseconds[,minutes[,hours[,weeks]]]]]])
delta2 = timedelta(10, 7611)
time3 = datetime.now() + delta2

#时分秒转秒数
#9354.0
seconds = timedelta(hours= 2, minutes= 35, seconds= 54).total_seconds()

