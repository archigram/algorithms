#顺序查找

def orderedSequentialSearch(L, item):
    '''顺序查找，L：有序列表'''
    pos = 0
    found = False  # 标记是否已找到数据项
    stop = False  # 如果遍历到的元素值比 item 大

    while pos < len(L) and not (found or stop):
        if L[pos] == item:
            found = True
        else:
            if L[pos] > item: stop = True
            else: pos += 1
    return found

if __name__ == '__main__':
    L1 = [17, 20, 26, 31, 44, 54, 55, 77, 93]
    item = 31
    print(f'{item} 是否在列表中: {orderedSequentialSearch(L1, item)}')
