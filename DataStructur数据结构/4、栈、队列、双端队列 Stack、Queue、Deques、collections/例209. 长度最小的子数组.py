#209. 长度最小的子数组
'''
给定一个含有 n 个正整数的数组和一个正整数 target 。
找出该数组中满足其和 ≥ target 的长度最小的连续子数组 [numsl, numsl+1, ..., numsr-1, numsr] ，
并返回其长度。如果不存在符合条件的子数组，返回 0 。
'''

#滑动窗口：以 j终止 的窗口指针后移动，当和大于目标时 向后移动以i为起始 的指针。
class Solution:
    def minSubArrayLen(self, s: int, nums: List[int]) -> int:
        lenf=len(nums)+1
        total=i=j=0
        while (j<len(nums)):
            total += nums[j]; j+=1
            while (total>=target):
                lenf=min(lenf,j-i)
                total -= nums[i]; i+=1
        if lenf==len(nums)+1: return 0
        else: return lenf