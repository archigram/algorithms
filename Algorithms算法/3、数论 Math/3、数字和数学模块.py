
#数字类型

# Number

from numbers import Number

# 判断一个值是否为数字
print(isinstance(13, Number))       # 整数
print(isinstance(2.65, Number))     # 小数
print(isinstance(5.4e45, Number))   # 科学计数法
print(isinstance(9+87j, Number))    # 复数
print(isinstance(-.09, Number))     # 负数
#True



# fractions 分数

from fractions import Fraction

# 创建

Fraction(16, -10)
# -8, 5
Fraction(123)
# 123, 1
Fraction()
# 0, 1
Fraction('3/7')
# 3, 7
Fraction(' -3/7 ')
# -3, 7
Fraction('1.414213 \t\n')
# 1414213, 1000000
Fraction('-.125')
#-1, 8
Fraction('7e-6')
#7, 1000000

Fraction(2.25)
#9, 4
Fraction(1.1)
#2476979795053773, 2251799813685248
from decimal import Decimal
Fraction(Decimal('1.1'))
#11, 10


from decimal import Decimal

a = Decimal(6)/Decimal(2)
# 2
b = Decimal(-7)/Decimal(2)
# -3.5
c = Decimal(-7)//Decimal(3)
# -2
-7//3 # -3
d = Decimal(-7)%Decimal(3)
# -1
-7%3 # 2

from decimal import Decimal

# 转分数
a = Decimal('-3.14').as_integer_ratio()
# (-157, 50)

# 用tuple输出符号、各位数字、小数位数
b = Decimal('-3.14').as_tuple()
# DecimalTuple(sign=1, digits=(3, 1, 4), exponent=-2)

# 小数位数
c = Decimal('1.41421356').quantize(Decimal('1.000'))
# 1.414