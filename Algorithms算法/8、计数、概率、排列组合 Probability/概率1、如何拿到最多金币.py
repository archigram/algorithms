import random
'''
方法功能:总共n 个房间，判断用指定的策略是否能拿到最多金币
返回值:如果能拿到返回True，否则返回False
'''

def getMaxNum(n):
    if n<1:
        return

    a = [random.uniform(1, n) for _ in range(n)]
    max4 = max(a[:4])

    for i in range(4,n-1):
        if a[i]>max4: #能拿到最多的金币
            return True
    return False # 不能拿到最多的金币

if __name__=="__main__":
    monitorCount = 1000
    success = 0
    for i in range(monitorCount):
        if getMaxNum(10):
            success +=1
    print(success/monitorCount)

