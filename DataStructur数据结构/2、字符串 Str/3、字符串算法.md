# 字符串算法

#### 1、判断两个字符串是否为换位字符串

利用ASCII创建字典统计每个字符数量

#### 2、判断括号表达式正确性

创建堆栈，从左到右读取一个括号串，遇到左括号入栈，遇到右括号出栈。

#### 3、最长回文子串

(1) Manacher 算法。时间复杂度是O(n)

(2) 算法：

​		在输入字符串s 的每个字符前后都添加# 作为分隔符，在整个字符串的首尾添加^ 和$字符。这样奇数长度或偶数长度的子串都会变为奇数。对于每个位置i，是否存在某个最长半径r，使得从i-r 到i+r 位置的子串是一个回文子串。

```python
'nonne','^#n#o#n#n#e#$','n#o#n','n#n'
```

​		优化：假如以j 为中心、p[j] 为半径的回文子串包含在以c 为中心、d-c 为半径的回文子串的前一半中，那么它一定也存在于后一半中。

```python
def manacher(s):
    assert ('$' not in s) and ('^' not in s) and ('#' not in s)
    if s == "": return(0, 1)
    t = "^#" + "#".join(s) + "#$"
    c = d = 0
    p = [0]*len(t)
    for i in range(1, len(t) - 1):
        # 相对于中心c 翻转下标i
        mirror = 2*c - i # = c-(i-c)
        p[i] = max(0, min(d-i, p[mirror]))
        # 增加以i 为中心的回文子串的长度
        while t[i + 1 + p[i]] == t[i - 1 - p[i]]:
            p[i] += 1
        # -- 必要时调整中心点
        if i + p[i] > d:
            c = i
            d = i + p[i]
    (k, i) = max((p[i], i) for i in range(1, len(t) - 1))
    return((i - k) // 2, (i + k) // 2) # 输出结果

s='abdghdgjiegeijgdg'
a=manacher(s)
print(s[a[0]:a[1]]) #dgjiegeijgd
```

